# REPORTE DE PERMISOS

![reporte icono](https://www.perkeso.gov.my/images/annual_report_icon.png)
___
## **Permisos:** Guest

![icono guest](https://d30y9cdsu7xlg0.cloudfront.net/png/25455-200.png)

* ACCION | DETALLE | MENSAJE
------------ | ------------- | -------------
Clone | Si
Crear rama local | si
Commit local |  si
Push | no |  *No estoy permitida a realizar push al código de proyecto*

___
## **Permisos:** Reporter
![icono reporter](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTojggn7Kgy4HJVzRstsKG70De-AsfRFfQCs8zUAdgm0CGCdlyC)

* ACCION | DETALLE | MENSAJE
------------ | ------------- | -------------
Clone | Si
Crear rama local | si
Crear rama remoto | no
Commit local |  si
Push | no |  *No estoy permitida a realizar push al código de ese proyecto*
Crear issue | si

___
## **Permisos:** Developer
![icono developer](https://cdn2.iconfinder.com/data/icons/characters-1/100/user_4_018-512.png)

* ACCION | DETALLE | MENSAJE
------------ | ------------- | -------------
Clone | Si
Crear rama local | si
Crear rama remoto | si
Crear branch desde otra branch | si
Commit local |  si
Push | si | 
Crear issue | si
Merge request | si
Request para branch de otra branch para master | si
Yo apruebo un merge request | si 
Merge request entre ramas | si | *pero solo entre ramas*
Merge request a rama master | no | *existe restricción*
Eliminar ramas | si
Rebase | si
Deshacer merge | si
Push a master | no | La rama esta protegida

___
## **Permisos:** Manteiner
![icono manteiner](https://www.linaro.org/assets/content/developerservices-icon-main.png)

* ACCION | DETALLE | MENSAJE
------------ | ------------- | -------------
Clone | Si
Crear rama local | si
Crear rama remoto | si
Crear branch desde otra branch | si
Commit local |  si
Push | si | 
Crear issue | si
Merge request | si
Request para branch de otra branch para master | si
Yo apruebo un merge request | no | *no aparezco como miembro para aprobacion*
Eliminar ramas | si
Rebase | si
Deshacer merge | si

### Observaciones
* No puedo ser yo misma quien aprueba un merge, pero si quien es asignada al merge
 :)



