# BIOGRAFÍA DE PAO STEPH

> ***Pao*** es una joven que actualmente, cursa sus semestres finales de la carrera de Ingeniería en Sistemas en la EPN. Amante de los *gatos* y *perros*, de una tarde en el cine con su novio, de bailar salsero o cualquier ritmo latino, y de su familia entera. Sueña con ser más grande, ya no en estatura, sino en la vida, y poder alcanzar varias metas y sueños, como viajar a *Brasil*. Ella es persistente, alegre en la medida de lo posible, buena amiga, y confianzuda. ![imagen portada](imagenes/biography-pao.jpg) 
